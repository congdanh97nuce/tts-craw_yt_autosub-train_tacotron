import os
import shutil
import sys

from youtube_dl import YoutubeDL

video_list_file = './playlists.txt'


def nomalize_name(input_str):
    s1 = u'ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚÝàáâãèéêìíòóôõùúýĂăĐđĨĩŨũƠơƯưẠạẢảẤấẦầẨẩẪẫẬậẮắẰằẲẳẴẵẶặẸẹẺẻẼẽẾếỀềỂểỄễỆệỈỉỊịỌọỎỏỐốỒồỔổỖỗỘộỚớỜờỞởỠỡỢợỤụỦủỨứỪừỬửỮữỰựỲỳỴỵỶỷỸỹ'
    s0 = u'AAAAEEEIIOOOOUUYaaaaeeeiioooouuyAaDdIiUuOoUuAaAaAaAaAaAaAaAaAaAaAaAaEeEeEeEeEeEeEeEeIiIiOoOoOoOoOoOoOoOoOoOoOoOoUuUuUuUuUuUuUuYyYyYyYy'
    s = ''
    remove_specialCharacter = ''.join(e for e in input_str if e.isalnum())
    # print(input_str.encode('utf-8'))
    for c in remove_specialCharacter:
        if c in s1:
            s += s0[s1.index(c)]
        else:
            s += c
    return s


ydl_opts = {

    'noplaylist': True,
    'extract-audio': True,
    'ignoreerrors': True,
    'nooverwrites': True,
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'wav',
        'preferredquality': '256'
    }],
    'postprocessor_args': [
        '-ar', '22050', '-acodec', 'pcm_s16le', "-ac", "1"
    ],
    'prefer_ffmpeg': True,
    'keepvideo': False,
    'outtmpl': 'output/temp/%(title)s.%(etx)s',
    'quiet': False

}


def get_raw_audio(video_list_file):
    print("Crawling raw audio from youtube")
    # Make audio_list
    video_list = []
    with open(video_list_file) as f:
        _video_list = f.readlines()
        for _video in _video_list:
            video = _video[:len(_video) - 2]
            video_list.append(video)
    # Make dircon
    # link audio khi dowload ve tu youtube
    if not os.path.exists('./audio/youtube'):
        os.makedirs('./audio/youtube')
    audio_path = './audio/youtube/'
    output_origin = './output/'
    # Download audio
    for video in video_list:
        with YoutubeDL(ydl_opts) as ydl:
            info_dict = ydl.extract_info(video, download=True)
            video_title = info_dict.get('title', None)

        source = output_origin + "temp/"

        if info_dict['extractor'] == "youtube:playlist":
            destination = audio_path + nomalize_name(video_title)
            shutil.move(source, destination)
        else:
            destination = audio_path
            files = os.listdir(source)
            for f in files:
                shutil.move(source + f, destination + f)

    if os.path.exists(output_origin):
        os.rmdir(output_origin)
    print("\n============================\n")
    print("Done Step1: Crawling raw audio from youtube")
    print("\n============================\n")


log = open("./output/" + "log.txt", 'w')
console = sys.stdout
sys.stdout = log

if __name__ == '__main__':
    get_raw_audio(video_list_file)
